buildscript {
    repositories {
        mavenCentral()
    }
}

plugins {
    application
    base
    idea
    java
    //`java-library`
    `maven-publish`
    signing
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    id("org.jetbrains.kotlin.jvm") version "1.3.72"
    id("org.jetbrains.kotlin.kapt") version "1.3.72"
    id("org.jetbrains.kotlin.plugin.spring") version "1.3.72"
    id("org.springframework.boot") version "2.3.4.RELEASE"
}

allprojects {
    group = "com.abc.chassis"
    version = "3.0.0"

    repositories {
        mavenCentral()
        maven { url = uri("https://plugins.gradle.org/m2/") }
        maven { url = uri("https://repo.spring.io/milestone") }
        maven { url = uri("https://dl.bintray.com/arrow-kt/arrow-kt/") }
        maven { url = uri("https://oss.jfrog.org/artifactory/oss-snapshot-local/") }
        jcenter()
    }

    tasks {
        withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
            println("Configuring Gradle Tasks KotlinCompile in project ${project.name}...")
            kotlinOptions {
                //allWarningsAsErrors = true // best practice
                apiVersion = "1.3"
                freeCompilerArgs += listOf("-Xjsr305=strict", "-Xopt-in=kotlin.ExperimentalUnsignedTypes")
                jvmTarget = "11"
                languageVersion = "1.3"
            }
        }

        withType<org.jetbrains.kotlin.gradle.dsl.KotlinCompile<*>> {
            println("Configuring Gradle DSL KotlinCompile in project ${project.name}...")
            kotlinOptions {
                //allWarningsAsErrors = true // best practice
                apiVersion = "1.3"
                freeCompilerArgs += listOf("-Xjsr305=strict", "-Xopt-in=kotlin.ExperimentalUnsignedTypes")
                languageVersion = "1.3"
            }
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }
}

java.sourceCompatibility = JavaVersion.VERSION_11

dependencies {
    implementation(project(":shared"))
    implementation(project(":plugins:default"))
    implementation(project(":plugins:flying"))
    implementation(project(":plugins:tracking"))

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.arrow-kt:arrow-core:0.11.0")
    implementation("io.arrow-kt:arrow-syntax:0.11.0")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("io.projectreactor:reactor-test")
}

tasks {
    jar { enabled = false }

    bootJar {
        enabled = true
        manifest {
            this.attributes(
                mapOf(
                    "Manifest-Version" to "1.0",
                    // Return the name of this package.
                    "Name" to group, "Created-By" to "ABOH", "Implementation-Title" to "Kewl",
                    "Implementation-Version" to "3.0.0", "Implementation-Vendor" to "ABOH",
                    // Requirements Document Specification
                    "Specification-Title" to "Kewl SRS", "Specification-Version" to 1.0,
                    "Specification-Vendor" to "ABOH", "Copyright" to "© 2020 - ABOH  All Rights Reserved.",
                    "Start-Class" to "com.abc.chassis.ChassisApplicationKt"
                )
            )
        }
    }
}
