rootProject.name = "chassis"

include(":plugins:default")
include(":plugins:flying")
include(":plugins:tracking")
include(":shared")
