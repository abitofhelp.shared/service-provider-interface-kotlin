# Chassis + Plugins

## Purpose
This is a webflux-fn service written in Kotlin and implementing the Java Service Provider Interface pattern. The application, "Chassis", will select one plugin provider at launch based on an environment variable.
