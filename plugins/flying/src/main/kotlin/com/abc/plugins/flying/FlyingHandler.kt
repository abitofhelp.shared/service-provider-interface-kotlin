package com.abc.plugins.flying

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse

interface FlyingHandler {
    suspend fun flying(serverRequest: ServerRequest): ServerResponse
}