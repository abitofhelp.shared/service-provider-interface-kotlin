package com.abc.plugins.flying

import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyValueAndAwait

@Component
class FlyingHandlerImpl(
    private val flyingService: FlyingService
) : FlyingHandler {
    override suspend fun flying(serverRequest: ServerRequest): ServerResponse =
        flyingService
            .flying()
            .fold({ throw it }, { msg ->
                ServerResponse
                    .ok()
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValueAndAwait(msg)
            })

}


