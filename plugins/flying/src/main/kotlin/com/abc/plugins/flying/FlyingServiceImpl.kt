package com.abc.plugins.flying

import arrow.core.Either
import org.springframework.stereotype.Service

@Service
class FlyingServiceImpl : FlyingService {
    override suspend fun flying(): Either<Throwable, Map<String, String>> =
        Either.Right(mapOf("say" to "Howdy from flying!"))
}