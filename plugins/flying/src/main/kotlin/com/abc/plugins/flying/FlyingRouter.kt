package com.abc.plugins.flying

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.coRouter

@Configuration
@ConditionalOnProperty(
    value= ["CSC.PLUGIN.NAME"],
    havingValue = "flying",
    matchIfMissing = false)
@ComponentScan(basePackages = ["com.abc.plugins.flying"])
class FlyingRouter {
    @Bean()
    fun flyingRoutes(flyingHandler: FlyingHandler): RouterFunction<ServerResponse> =
        coRouter {
            accept(MediaType.APPLICATION_JSON).nest {
                ("/api".nest {
                    GET("/flying", flyingHandler::flying)
                })
            }
        }
}
