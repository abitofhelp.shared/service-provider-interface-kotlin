package com.abc.plugins.flying

import arrow.core.Either

interface FlyingService {
    suspend fun flying(): Either<Throwable, Map<String, String>>
}