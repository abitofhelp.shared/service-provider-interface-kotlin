package com.abc.plugins.flying

import com.abc.shared.PluginServiceProvider
import com.abc.shared.PluginServiceProviderType
import com.abc.shared.PluginServiceProviderType.Companion.toPluginServiceProviderTypeString
import org.springframework.stereotype.Component

@Component
class FlyingServiceProvider : PluginServiceProvider {
    override val name: String
        get() = PluginServiceProviderType.FLYING.toPluginServiceProviderTypeString()
    override val kind: PluginServiceProviderType
        get() = PluginServiceProviderType.FLYING
    override val fqn: String
        get() = FlyingServiceProvider::class.java.name
    override val pkg: String
        get() = fqn.substringBeforeLast(".")
}
