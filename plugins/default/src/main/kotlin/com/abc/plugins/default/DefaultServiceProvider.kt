package com.abc.plugins.default

import com.abc.shared.PluginServiceProvider
import com.abc.shared.PluginServiceProviderType
import com.abc.shared.PluginServiceProviderType.Companion.toPluginServiceProviderTypeString
import org.springframework.stereotype.Component

@Component
class DefaultServiceProvider : PluginServiceProvider {
    override val name: String
        get() = PluginServiceProviderType.DEFAULT.toPluginServiceProviderTypeString()
    override val kind: PluginServiceProviderType
        get() = PluginServiceProviderType.DEFAULT
    override val fqn: String
        get() = DefaultServiceProvider::class.java.name
    override val pkg: String
        get() = fqn.substringBeforeLast(".")
}
