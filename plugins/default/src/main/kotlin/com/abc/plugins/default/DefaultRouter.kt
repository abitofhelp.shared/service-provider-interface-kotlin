package com.abc.plugins.default

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.coRouter

@Configuration
@ConditionalOnProperty(
    value= ["CSC.PLUGIN.NAME"],
    havingValue = "default",
    matchIfMissing = false)
@ComponentScan(basePackages = ["com.abc.plugins.default"])
class DefaultRouter {
    @Bean()
    fun defaultRoutes(): RouterFunction<ServerResponse> =
    coRouter {
        accept(MediaType.APPLICATION_JSON).nest {
            ("/api".nest {
                GET("/default") { r ->
                    ok().bodyValueAndAwait("Howdy from default")
                }
            })
        }
    }
}
