buildscript {
    repositories {
        mavenCentral()
    }
}

plugins {
    base
    idea
    java
    `java-library`
    `maven-publish`
    signing
    id("io.spring.dependency-management")
    id("org.jetbrains.kotlin.jvm")
    id("org.jetbrains.kotlin.kapt")
    id("org.jetbrains.kotlin.plugin.spring")
    id("org.springframework.boot")
}

dependencies {
    implementation(project(":shared"))
    //implementation(project(":plugins:tracking"))

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.arrow-kt:arrow-core:0.11.0")
    implementation("io.arrow-kt:arrow-syntax:0.11.0")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("io.projectreactor:reactor-test")
}

tasks {
    jar {
        enabled = true
        manifest {
            this.attributes(
                mapOf(
                    "Manifest-Version" to "1.0",
                    // Return the name of this package.
                    "Name" to group, "Created-By" to "ABOH", "Implementation-Title" to "Kewl",
                    "Implementation-Version" to "3.0.0", "Implementation-Vendor" to "ABOH",
                    // Requirements Document Specification
                    "Specification-Title" to "Kewl SRS", "Specification-Version" to 1.0,
                    "Specification-Vendor" to "ABOH", "Copyright" to "© 2020 - ABOH  All Rights Reserved."
                )
            )
        }
    }

    bootJar { enabled = false }
}