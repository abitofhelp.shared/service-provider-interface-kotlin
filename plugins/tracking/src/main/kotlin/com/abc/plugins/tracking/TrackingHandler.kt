package com.abc.plugins.tracking

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse

interface TrackingHandler {
    suspend fun tracking(serverRequest: ServerRequest): ServerResponse
}