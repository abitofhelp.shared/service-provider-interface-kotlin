package com.abc.plugins.tracking

import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyValueAndAwait

@Component
class TrackingHandlerImpl(
    private val trackingService: TrackingService
) : TrackingHandler {
    override suspend fun tracking(serverRequest: ServerRequest): ServerResponse =
        trackingService
            .tracking()
            .fold({ throw it }, { msg ->
                ServerResponse
                    .ok()
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValueAndAwait(msg)
            })

}


