package com.abc.plugins.tracking

import com.abc.shared.PluginServiceProvider
import com.abc.shared.PluginServiceProviderType
import com.abc.shared.PluginServiceProviderType.Companion.toPluginServiceProviderTypeString
import org.springframework.stereotype.Component

@Component
class TrackingServiceProvider : PluginServiceProvider {
    override val name: String
        get() = PluginServiceProviderType.TRACKING.toPluginServiceProviderTypeString()
    override val kind: PluginServiceProviderType
        get() = PluginServiceProviderType.TRACKING
    override val fqn: String
        get() = TrackingServiceProvider::class.java.name
    override val pkg: String
        get() = fqn.substringBeforeLast(".")
}
