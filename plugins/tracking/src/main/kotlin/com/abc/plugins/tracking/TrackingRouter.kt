package com.abc.plugins.tracking

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.coRouter

@Configuration
@ConditionalOnProperty(
    value= ["CSC.PLUGIN.NAME"],
    havingValue = "tracking",
    matchIfMissing = false)
@ComponentScan(basePackages = ["com.abc.plugins.tracking"])
class TrackingRouter {
    @Bean()
    fun trackerRoutes(trackingHandler: TrackingHandler): RouterFunction<ServerResponse> =
        coRouter {
            accept(MediaType.APPLICATION_JSON).nest {
                ("/api".nest {
                    GET("/tracking", trackingHandler::tracking)
                })
            }
        }
}
