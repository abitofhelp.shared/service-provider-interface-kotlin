package com.abc.plugins.tracking

import arrow.core.Either
import org.springframework.stereotype.Service

@Service
class TrackingServiceImpl : TrackingService {
    override suspend fun tracking(): Either<Throwable, Map<String, String>> =
        Either.Right(mapOf("say" to "Howdy from tracking!"))
}