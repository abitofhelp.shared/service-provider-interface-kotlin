package com.abc.plugins.tracking

import arrow.core.Either

interface TrackingService {
    suspend fun tracking(): Either<Throwable, Map<String, String>>
}