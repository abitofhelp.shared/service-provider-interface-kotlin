package com.abc.shared

// SPI for the plugin system for the Chassis web application.
interface PluginServiceProvider {
    val kind: PluginServiceProviderType
    val name: String
    val fqn: String
    val pkg: String
}