package com.abc.shared

/**
 * The list of known plugin service provider interface adapters
 */
enum class PluginServiceProviderType(val providerType: String) {
    DEFAULT("Default"),           // THE DEFAULT ADAPTER
    FLYING("Flying"),
    TRACKING("Tracking");

    companion object {
        /**
         * Get an enum value from a String value.
         */
        private val values = values()

        // Throw an exception if the value is not found in the enumeration.
        fun getByValue(value: String) =
            values.first { it.name == value }

        fun PluginServiceProviderType.toPluginServiceProviderTypeString() =
            this.providerType
    }
}
