package com.abc.chassis

import com.abc.shared.PluginServiceProvider
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.annotation.ComponentScan
import java.util.*

@ComponentScan(basePackages = [
    "com.abc.chassis"
    ,"com.abc.plugins.default"
    ,"com.abc.plugins.flying"
    ,"com.abc.plugins.tracking"
])
@SpringBootApplication
class ChassisApplication: CommandLineRunner {
    override fun run(vararg args: String?) {

        val services = ServiceLoader.load(PluginServiceProvider::class.java)
        val cnt = services.count()
        for (service in services) {
        	println("Plugin '${service.name}' in package '${service.pkg}' has fqn '${service.fqn}'")

        }
    }
}

fun main(args: Array<String>) {
    runApplication<ChassisApplication>(*args)
}

